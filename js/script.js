document.getElementById('stop-time').style.display = 'none';
document.getElementById('clear-all').style.display = 'none';
document.getElementById('show-time').style.display = 'inline-block';

document.getElementById('show-time').onclick = () => {
    startTime();
}
document.getElementById('stop-time').onclick = () => {
    stopTime();
    calculateTime();
    calculatePayment();
}
document.getElementById('clear-all').onclick = () => {
    clear();
}

var splitStartTime
var splitSecondStart

function startTime() {
    let nowToday = new Date();
    let timeStart = nowToday.toLocaleTimeString()
    splitStartTime = timeStart.split(":")
    document.getElementById('start-zero').innerHTML = timeStart
    document.getElementById('show-time').style.display = 'none'
    document.getElementById('stop-time').style.display = 'inline-block'
    splitSecondStart = splitStartTime[2].split(" ")
}

var splitStopTime
var splitSecondStop

function stopTime() {
    let nowToday = new Date();
    let timeStop = nowToday.toLocaleTimeString()
    splitStopTime = timeStop.split(":")
    document.getElementById('end-time').innerHTML = timeStop
    document.getElementById('stop-time').style.display = 'none'
    document.getElementById('clear-all').style.display = 'inline-block'
    splitSecondStop = splitStopTime[2].split(" ")
}

var totalMn

function calculateTime() {
    let nowToday = new Date();
    let ca = Intl.DateTimeFormat('en-CA').format(nowToday)

    let startTime = new Date(ca + " " + splitStartTime[0] + ":" + splitStartTime[1] + ":" + splitSecondStart[0])
    let getStartTime = startTime.toLocaleDateString().split(":")

    let stopTime = new Date(ca + " " + splitStopTime[0] + ":" + splitStopTime[1] + ":" + splitSecondStop[0])
    let getStopTime = stopTime.toLocaleDateString().split(":")

    let totalTime = stopTime - startTime;
    totalMn = Math.floor(totalTime / 60000);
    document.getElementById('total-time').innerHTML = totalMn;

}

function calculatePayment() {
    let totalPayment
        // totalMn = 180;
        // document.getElementById('total-time').innerHTML = totalMn;
    if (totalMn >= 0 && totalMn <= 15) {
        totalPayment = 500;
    } else if (totalMn > 16 && totalMn <= 30) {
        totalPayment = 1000;
    } else if (totalMn > 31 && totalMn <= 60) {
        totalPayment = 1500;
    } else {
        let lastMn1 = Math.floor(totalMn / 60);
        let tempPay = lastMn1 * 1500;
        let lastMn2 = Math.floor(totalMn % 60);
        if (lastMn2 >= 0 && lastMn2 <= 15) {
            totalPayment = tempPay + 500;
        } else if (lastMn2 > 16 && lastMn2 <= 30) {
            totalPayment = tempPay + 1000;
        } else if (lastMn2 > 31 && lastMn2 <= 60) {
            totalPayment = tempPay + 1500;
        }
    }
    document.getElementById('pay').innerHTML = totalPayment;
}

function clear() {
    document.getElementById('start-zero').innerHTML = '0:00:00';
    document.getElementById('end-time').innerHTML = '0:00:00';
    document.getElementById('total-time').innerHTML = '0';
    document.getElementById('pay').innerHTML = '0';
    document.getElementById('clear-all').style.display = 'none';
    document.getElementById('show-time').style.display = 'inline-block';
}

let nowTime = setInterval(() => {
    let nowToday = new Date();
    let newDay = nowToday.toString().split(" ")

    let startDate = newDay[0] + " " + newDay[1] + " " + newDay[2] + " " + newDay[3];
    let startTime = nowToday.toLocaleTimeString();

    document.getElementById('live-time').innerHTML = " " + startDate + " " + startTime;

}, 1000)